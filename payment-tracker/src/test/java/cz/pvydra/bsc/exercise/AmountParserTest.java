package cz.pvydra.bsc.exercise;

import javax.money.Monetary;
import javax.money.MonetaryAmount;

import org.javamoney.moneta.Money;
import org.junit.Test;

import cz.pvydra.bsc.exercise.util.AmountParser;
import cz.pvydra.bsc.exercise.util.InvalidAmountException;

public class AmountParserTest {

    @Test
    public void parserOkInteger() {

        String input1 = "USD 200";
        MonetaryAmount ref1 = Money.of(200d, Monetary.getCurrency("USD"));

        MonetaryAmount p1 = null;
        try {
            p1 = AmountParser.parseAmount(input1);
        } catch (InvalidAmountException e) {
            // TODO Auto-generated catch block

        }

        assert (ref1.equals(p1));

    }

    @Test
    public void parserOkDouble() {

        String input1 = "USD 200.05";
        MonetaryAmount ref1 = Money.of(200.05d, Monetary.getCurrency("USD"));

        MonetaryAmount p1 = null;
        try {
            p1 = AmountParser.parseAmount(input1);
        } catch (InvalidAmountException e) {
            // TODO Auto-generated catch block

        }

        assert (ref1.equals(p1));

    }

    @Test(expected = InvalidAmountException.class)
    public void parserLowerCaseFail() throws InvalidAmountException {
        AmountParser.parseAmount("abc 100");
    }

    @Test(expected = InvalidAmountException.class)
    public void parserNumberFail() throws InvalidAmountException {
        AmountParser.parseAmount("ABC 10a");
    }

}

package cz.pvydra.bsc.exercise.logic.impl;

import java.util.List;

import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;

import cz.pvydra.bsc.exercise.input.api.IPaymentSource;
import cz.pvydra.bsc.exercise.logic.api.IPaymentProcessor;
import cz.pvydra.bsc.exercise.model.Balance;
import cz.pvydra.bsc.exercise.model.Payment;
import cz.pvydra.bsc.exercise.store.api.IPaymentStore;
import cz.pvydra.bsc.exercise.store.factory.PaymentStoreFactory;
import cz.pvydra.bsc.exercise.util.CurrencyConvertor;

/**
 *
 * @author petr.vydra
 *
 */
public class PaymentProcessor implements IPaymentProcessor {

    private final IPaymentStore paymentStore = PaymentStoreFactory.getInstance();

    private final CurrencyUnit targetCurrency;

    public PaymentProcessor(CurrencyUnit targetCurrency) {
        this.targetCurrency = targetCurrency;
    }

    @Override
    public String printBalances() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");
        paymentStore.getAllCurrencies().forEach(currency -> {

            Balance balance = paymentStore.getBalanceForCurrency(currency);

            MonetaryAmount balanceAmount = balance.getSum();
            sb.append(balanceAmount.toString());

            MonetaryAmount convertedAmount = CurrencyConvertor.convert(balanceAmount, targetCurrency);

            if (convertedAmount != null) {
                sb.append(" (");
                sb.append(convertedAmount.toString());
                sb.append(")");
            }
            sb.append("\n");
        });

        return sb.toString();
    }

    @Override
    public void accountPayment(final Payment payment) {        
        paymentStore.addPayment(payment);
    }

    /**
     * Load predefined list of payments from {@link IPaymentSource}
     * 
     * @param source
     */
    @Override
    public void loadPayments(final IPaymentSource source) {
        
        List<Payment> paymentList = source.loadPayments();
        if (paymentList == null) {
            return;
        }

        paymentList.forEach(payment -> paymentStore.addPayment(payment));

    }

}

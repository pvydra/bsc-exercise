package cz.pvydra.bsc.exercise.util;

import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;

import org.javamoney.moneta.Money;

import cz.pvydra.bsc.exercise.store.api.IConversionRateStore;
import cz.pvydra.bsc.exercise.store.factory.ConversionRateStoreFactory;

/**
 * Converts value of {@link MonetaryAmount} to target {@link CurrencyUnit}
 *
 * @author petr.vydra
 */
public class CurrencyConvertor {

    private CurrencyConvertor() {
    }

    /**
     * Converts {@link MonetaryAmount} to Amount int target {@link CurrencyUnit}.
     * Uses {@link IConversionRateStore} provided by @{link ConversionRateStoreFactory}
     * for determining converion rate
     * 
     * @param amount
     * @param targetCurrency
     * @return converted {@link MonetaryAmount} in target {@link CurrencyUnit} or null if
     * conversion rate is not found for given currencies.
     */
    public static MonetaryAmount convert(MonetaryAmount amount, CurrencyUnit targetCurrency) {
        // no target currency specified
        if (targetCurrency == null) {
            return null;
        }

        // if original and target currency are the same, don't convert
        if (amount.getCurrency().equals(targetCurrency)) {
            return null;
        }

        IConversionRateStore crStore = ConversionRateStoreFactory.getInstance();

        Number rate = crStore.getRateForCurrency(amount.getCurrency(), targetCurrency);
        // 
        if (rate == null) {
            // no conversion value for conversion from original to target currency
            return null;
        } else {
            return Money.of(amount.getNumber().doubleValue() / rate.doubleValue(), targetCurrency);
        }

    }
}

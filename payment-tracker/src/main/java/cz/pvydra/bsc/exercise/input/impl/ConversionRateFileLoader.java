package cz.pvydra.bsc.exercise.input.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.pvydra.bsc.exercise.input.api.IConversionRateSource;
import cz.pvydra.bsc.exercise.util.AmountParser;
import cz.pvydra.bsc.exercise.util.InvalidAmountException;
import cz.pvydra.bsc.exercise.util.Messages;

public class ConversionRateFileLoader implements IConversionRateSource {

    private final Logger logger = LoggerFactory.getLogger(IConversionRateSource.class);

    private final Path inputFile;

    public ConversionRateFileLoader(final String path) {
        if (path != null) {
            inputFile = Paths.get(path);
        } else {
            inputFile = null;
        }
    }

    @Override
    public Map<CurrencyUnit, Number> loadConversionMap() {
        Map<CurrencyUnit, Number> conversionMap = new HashMap<>();

        if (inputFile != null && Files.exists(inputFile)) {

            try (Stream<String> stream = Files.lines(inputFile)) {
                stream.forEach(inputLine -> {
                    try {
                        MonetaryAmount p = AmountParser.parseAmount(inputLine);
                        conversionMap.put(p.getCurrency(), p.getNumber());
                    } catch (InvalidAmountException ex) {
                        logger.error(Messages.CONVERSION_RATE_LINE_FAILED.getMessage(), ex);
                    }
                });
                return conversionMap;
            } catch (IOException ex) {
                logger.error(Messages.CONVERSION_RATE_FILE_FAILED.getMessage(), ex);

            }
        }
        return conversionMap;
    }

}

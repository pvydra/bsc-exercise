package cz.pvydra.bsc.exercise.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.money.CurrencyUnit;
import javax.money.MonetaryAmount;

import org.javamoney.moneta.Money;

/**
 *
 * @author petr.vydra
 */
public class Balance {


    private final List<Payment> payments;

    private MonetaryAmount sum;


    private Balance(CurrencyUnit currency) {
        this.payments = new ArrayList<>();
        this.sum = Money.of(BigDecimal.ZERO, currency);
    }

    public static Balance of(CurrencyUnit currency) {
        return new Balance(currency);
    }

    public List<Payment> getPaymensts() {
        return payments;
    }

    public MonetaryAmount getSum() {
        return sum;
    }

    public void add(Payment increasedBy) {
        if (increasedBy == null) {
            return;
        }
        if (!increasedBy.getAmount().getCurrency().equals(this.getSum().getCurrency())) {
            throw new IllegalArgumentException();
        }
        this.payments.add(increasedBy);
        this.sum = sum.add(increasedBy.getAmount());

    }

   
    
    

}

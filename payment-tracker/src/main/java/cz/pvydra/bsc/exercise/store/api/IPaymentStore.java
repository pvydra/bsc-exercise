package cz.pvydra.bsc.exercise.store.api;

import java.util.Set;

import javax.money.CurrencyUnit;

import cz.pvydra.bsc.exercise.model.Balance;
import cz.pvydra.bsc.exercise.model.Payment;

/**
 *
 * @author petr.vydra
 *
 */
public interface IPaymentStore {

    /**
     * Adds {@link Payment} to store. Added {@code payment} is accounted to
     * total balance of all payments in {@link CurrencyUnit} of {@code payment}
     *
     * @param payment
     * @return true if {@code payment} has been succesfully added.
     */
    boolean addPayment(Payment payment);

    /**
     * Gets {@link Balance} from store for given {@link CurrencyUnit}.
     *
     * @param currency
     * @return Balance for given currency
     */
    Balance getBalanceForCurrency(CurrencyUnit currency);

    /**
     * Gets all of {@link CurrencyUnit} from store.
     *
     * @return Set of all currencies in store
     */
    Set<CurrencyUnit> getAllCurrencies();

}

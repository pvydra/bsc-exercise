package cz.pvydra.bsc.exercise.input.api;

import cz.pvydra.bsc.exercise.model.Payment;
import java.util.List;

/**
 *  Source for predefined payments
 * 
 * @author petr.vydra
 */
public interface IPaymentSource {

    /**
     * Load List of {@link Payment} from source
     * 
     * @return list of loaded payments
     */
    List<Payment> loadPayments();
}

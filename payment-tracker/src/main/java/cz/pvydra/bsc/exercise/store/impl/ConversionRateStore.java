package cz.pvydra.bsc.exercise.store.impl;

import java.util.Map;

import javax.money.CurrencyUnit;

import cz.pvydra.bsc.exercise.input.api.IConversionRateSource;
import cz.pvydra.bsc.exercise.store.api.IConversionRateStore;

/**
 * Singleton implementation of @{link IConversionRateStore}.
 * 
 * @author petr.vydra
 */
public enum ConversionRateStore implements IConversionRateStore {

    INSTANCE;

    private Map<CurrencyUnit, Number> rates;

    @Override
    public Number getRateForCurrency(final CurrencyUnit original, final CurrencyUnit target) {

        if (rates != null) {
            return rates.get(original);
        }
        return null;
    }

    @Override
    public void init(final IConversionRateSource source) {
        
        Map<CurrencyUnit, Number> map = source.loadConversionMap();

        this.rates = map;

    }

}

package cz.pvydra.bsc.exercise.model;

import java.util.Date;
import java.util.Objects;

import javax.money.MonetaryAmount;

/**
 *  Payment
 * 
 * @author petr.vydra
 *
 */
public final class Payment {

    private final MonetaryAmount amount;

    private final Date originalDate;

    private Payment(MonetaryAmount amount, Date originalDate) {
        this.amount = amount;
        this.originalDate = originalDate;
    }

    public static Payment get(final MonetaryAmount amount, final Date originalDate) {
        if (amount == null) {
            throw new IllegalArgumentException("Amount mustn't be null");
        }
        if (originalDate == null) {
            throw new IllegalArgumentException("OriginalDate mustn't be null");
        }
        return new Payment(amount, originalDate);
    }

    public Date getOriginalDate() {
        return new Date(originalDate.getTime());
    }

    public MonetaryAmount getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return amount.toString();
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Payment other = (Payment) obj;
        if (!Objects.equals(this.amount, other.amount)) {
            return false;
        }
        if (!Objects.equals(this.originalDate, other.originalDate)) {
            return false;
        }
        return true;
    }

}

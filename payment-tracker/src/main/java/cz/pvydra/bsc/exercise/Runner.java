package cz.pvydra.bsc.exercise;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Date;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.pvydra.bsc.exercise.input.api.IConversionRateSource;
import cz.pvydra.bsc.exercise.input.api.IPaymentSource;
import cz.pvydra.bsc.exercise.input.impl.ConversionRateFileLoader;
import cz.pvydra.bsc.exercise.input.impl.PaymentFileLoader;
import cz.pvydra.bsc.exercise.logic.impl.PaymentProcessor;
import cz.pvydra.bsc.exercise.model.Payment;
import cz.pvydra.bsc.exercise.store.factory.ConversionRateStoreFactory;
import cz.pvydra.bsc.exercise.util.AmountParser;
import cz.pvydra.bsc.exercise.util.InvalidAmountException;

/**
 * 
 * @author petr.vydra
 */
public class Runner {

    private static final Logger logger = LoggerFactory.getLogger(Runner.class);

    private static final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

    private static final int PRINTOUT_PERIOD = 5; // [s]
    private static final int PRINTOUT_DELAY = 0;

    private static final String EXIT_COMMAND = "quit";

    private Runner() {

    }

    /**
     * Old good main method
     * 
     * @param args
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {

        String conversionTargetCurrencyCode = null;
        if (args.length > 1) {
            conversionTargetCurrencyCode = args[1];

        }

        String paymentsFilename = null;
        if (args.length > 0) {
            paymentsFilename = args[0];

        }

        runner(System.in, System.out, conversionTargetCurrencyCode, paymentsFilename);
    }

    public static void runner(InputStream in, PrintStream out, String conversionTargetCurrencyCode, String paymentsFilename) {

         CurrencyUnit conversionTargetCurrency = null;
        
        // load conversion rate file
        if (conversionTargetCurrencyCode != null) {           
            conversionTargetCurrency = Monetary.getCurrency(conversionTargetCurrencyCode);
            
            IConversionRateSource crSource = new ConversionRateFileLoader(conversionTargetCurrencyCode + ".txt");
            ConversionRateStoreFactory.getInstance().init(crSource);
        }

        PaymentProcessor paymentProcessor = new PaymentProcessor(conversionTargetCurrency);
        // load payment file
        IPaymentSource source = new PaymentFileLoader(paymentsFilename);        
        paymentProcessor.loadPayments(source);

        
        Runnable task = () -> out.println(paymentProcessor.printBalances());
        scheduler.scheduleAtFixedRate(task, PRINTOUT_DELAY, PRINTOUT_PERIOD, TimeUnit.SECONDS);

        try (Scanner scanner = new Scanner(System.in)) {
            while (scanner.hasNext()) {

                String inputLine = scanner.nextLine();

                if (inputLine == null) {
                    continue;
                }

                if (EXIT_COMMAND.equalsIgnoreCase(inputLine)) {
                    scheduler.shutdown();//                    
                    break;
                }
                try {
                    MonetaryAmount amount = AmountParser.parseAmount(inputLine);
                    Payment payment = Payment.get(amount, new Date());
                    paymentProcessor.accountPayment(payment);
                    logger.info("Ok");
                } catch (InvalidAmountException e) {
                    logger.error(e.getMessage(), e);
                }

            }

        }

        logger.info("Bye!");
        System.exit(0);

    }
}

package cz.pvydra.bsc.exercise.util;

/**
 *
 * @author petr.vydra
 *
 */
public class InvalidAmountException extends Exception {

    private static final long serialVersionUID = -6639839333555188383L;

    public InvalidAmountException(String message) {
        super(message);
    }

    public InvalidAmountException(String message, Throwable th) {
        super(message, th);
    }

    public InvalidAmountException(Throwable th) {
        super(th);
    }

    public InvalidAmountException() {
        super();
    }

}

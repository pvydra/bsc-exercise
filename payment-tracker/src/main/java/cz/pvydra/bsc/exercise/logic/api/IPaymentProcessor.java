package cz.pvydra.bsc.exercise.logic.api;

import cz.pvydra.bsc.exercise.input.api.IPaymentSource;
import cz.pvydra.bsc.exercise.model.Balance;
import cz.pvydra.bsc.exercise.model.Payment;

/**
 * Processor for {@link Payment}
 *
 * @author petr.vydra
 */
public interface IPaymentProcessor {

    /**
     * 
     * 
     * @return String of {@link Balance}
     */
    String printBalances();

    /**
     * Account {@code payment} to {@link Balance}
     * 
     * @param payment 
     */
    void accountPayment(Payment payment);

    /**
     * Load {@link Payment} from {@code source}
     * 
     * @param source 
     */
    void loadPayments(IPaymentSource source);
}

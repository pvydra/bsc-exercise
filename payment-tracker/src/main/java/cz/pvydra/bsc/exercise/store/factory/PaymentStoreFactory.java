package cz.pvydra.bsc.exercise.store.factory;

import cz.pvydra.bsc.exercise.store.api.IPaymentStore;
import cz.pvydra.bsc.exercise.store.impl.PaymentStore;

/**
 *  Factory for {@link IPaymentStore}
 * 
 * @author petr.vydra
 *
 */
public class PaymentStoreFactory {

    private PaymentStoreFactory() {
    }

    /**
     * Factory method
     * 
     * @return IPaymentStore instance
     */
    public static IPaymentStore getInstance() {
        return PaymentStore.INSTANCE;
    }
}

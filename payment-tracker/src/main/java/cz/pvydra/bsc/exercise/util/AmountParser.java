package cz.pvydra.bsc.exercise.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.money.CurrencyContext;
import javax.money.CurrencyContextBuilder;
import javax.money.CurrencyUnit;
import javax.money.Monetary;
import javax.money.MonetaryAmount;
import javax.money.UnknownCurrencyException;

import org.javamoney.moneta.CurrencyUnitBuilder;
import org.javamoney.moneta.Money;

/**
 * Class responsible for converting text input to {@link MonetaryAmount}
 *
 * @author petr.vydra
 *
 */
public class AmountParser {

    private static final CurrencyContext CURRENCY_CONTEXT = CurrencyContextBuilder.of("").build();

    /**
     * Regular expression for parsing input text
     */
    private static final Pattern INPUT_LINE_PATTERN = Pattern.compile("^([A-Z]{3}) (-?\\d+\\.?\\d*)$");

    private AmountParser() {
        
    }
    
    /**
     * Method converting text input to {@link MonetaryAmount} object using regular
     * expression.
     *
     *
     * @param inputLine
     * @return
     * @throws InvalidAmountException
     */
    public static MonetaryAmount parseAmount(String inputLine) throws InvalidAmountException {

        Matcher matcher = INPUT_LINE_PATTERN.matcher(inputLine);
        if (matcher.matches()) {
            String currencyCodeString = matcher.group(1);
            String amountString = matcher.group(2);
            Double amount = null;
            
            try {
                amount = Double.valueOf(amountString);
            } catch (NumberFormatException nfe) {
                throw new InvalidAmountException(Messages.PARSER_INPUT_ERROR.getMessage() + inputLine, nfe);
            }


            CurrencyUnit currency = null;
            try {
                currency = Monetary.getCurrency(currencyCodeString);
            } catch (UnknownCurrencyException uce) {
                currency = CurrencyUnitBuilder.of(currencyCodeString, CURRENCY_CONTEXT).build();
            }
            return Money.of(amount, currency);

        }
        throw new InvalidAmountException(Messages.INVALID_TEXT_INPUT.getMessage() + " " + inputLine );

    }
}

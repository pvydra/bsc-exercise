package cz.pvydra.bsc.exercise.input.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

import javax.money.MonetaryAmount;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cz.pvydra.bsc.exercise.input.api.IPaymentSource;
import cz.pvydra.bsc.exercise.model.Payment;
import cz.pvydra.bsc.exercise.util.AmountParser;
import cz.pvydra.bsc.exercise.util.InvalidAmountException;

/**
 *
 * Loads predefined payments from file
 *
 * @author petr.vydra
 *
 */
public class PaymentFileLoader implements IPaymentSource {

    private final Logger logger = LoggerFactory.getLogger(PaymentFileLoader.class);

    private final Path inputFile;

    public PaymentFileLoader(final String path) {
        if (path != null) {
            inputFile = Paths.get(path);
        } else {
            inputFile = null;
        }
    }

    @Override
    public List<Payment> loadPayments() {
        List<Payment> paymentList = new ArrayList<>();

        if (inputFile != null && Files.exists(inputFile)) {
            Date paymentDate = new Date();
            try (Stream<String> stream = Files.lines(inputFile)) {
                stream.forEach(inputLine -> {
                    try {
                        MonetaryAmount amount = AmountParser.parseAmount(inputLine);
                        Payment payment = Payment.get(amount, paymentDate);
                        paymentList.add(payment);
                    } catch (InvalidAmountException | IllegalArgumentException e) {
                        logger.error("", e);
                    }
                });
                return paymentList;
            } catch (IOException e) {
                logger.error("", e);
            }
        }
        return paymentList;
    }

}

package cz.pvydra.bsc.exercise.util;

/**
 * Messages enums
 * 
 * @author petr.vydra
 *
 */
public enum Messages {

	INVALID_TEXT_INPUT("Invalid input"), 
        CONVERSION_RATE_FILE_FAILED("Conversion rate file load failed"),
	CONVERSION_RATE_LINE_FAILED("Conversion rate parse error on line"), 
        PARSER_INPUT_ERROR("Parser input error: ");
        
        
	private final String message;
	
	private Messages(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	
}

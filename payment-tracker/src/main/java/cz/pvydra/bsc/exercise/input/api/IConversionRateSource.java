package cz.pvydra.bsc.exercise.input.api;

import java.util.Map;

import javax.money.CurrencyUnit;

/**
 *  
 * 
 * @author petr.vydra
 */
public interface IConversionRateSource {
        
    Map<CurrencyUnit, Number> loadConversionMap();
}

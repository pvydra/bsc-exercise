package cz.pvydra.bsc.exercise.store.factory;

import cz.pvydra.bsc.exercise.store.api.IConversionRateStore;
import cz.pvydra.bsc.exercise.store.impl.ConversionRateStore;

/**
 *  Factory for {@link IConversionRateStore}
 * 
 * @author petr.vydra
 *
 */
public class ConversionRateStoreFactory {

    private ConversionRateStoreFactory() {
    }

    /**
     * Factory method
     * 
     * @return IConversionRateStore instance
     */
    public static IConversionRateStore getInstance() {
        return ConversionRateStore.INSTANCE;
    }
}

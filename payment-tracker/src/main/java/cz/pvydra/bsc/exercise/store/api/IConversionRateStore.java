package cz.pvydra.bsc.exercise.store.api;

import javax.money.CurrencyUnit;

import cz.pvydra.bsc.exercise.input.api.IConversionRateSource;

/**
 *
 * @author petr.vydra
 *
 */
public interface IConversionRateStore {

    Number getRateForCurrency(final CurrencyUnit original, final CurrencyUnit target);

    void init(final IConversionRateSource source);

}

package cz.pvydra.bsc.exercise.store.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.money.CurrencyUnit;

import cz.pvydra.bsc.exercise.model.Balance;
import cz.pvydra.bsc.exercise.model.Payment;
import cz.pvydra.bsc.exercise.store.api.IPaymentStore;

/**
 * Singleton implementation of @{link IPaymentStore}.
 *
 * @author petr.vydra
 *
 */
public enum PaymentStore implements IPaymentStore {

    INSTANCE;

    private final Map<CurrencyUnit, Balance> payments = new HashMap<>();

    /**
     * Adds {@link Payment} to store. Added {@code payment} is accounted to
     * total balance of all payments in {@link CurrencyUnit} of {@code payment}
     * Method is synchronized.
     *
     * @param payment
     * @return true if {@code payment} has been succesfully added.
     */
    @Override
    public boolean addPayment(final Payment payment) {
        synchronized (payments) {
            CurrencyUnit currency = payment.getAmount().getCurrency();

            Balance balance = payments.get(currency);
            if (balance == null) {

                balance = Balance.of(currency);
                payments.put(currency, balance);
            }

            balance.add(payment);

            return true;
        }

    }

    /**
     * Gets {@link Balance} from store for given {@link CurrencyUnit}.
     * Method is synchronized.
     *
     * @param currency
     * @return Balance for given currency
     */
    @Override
    public Balance getBalanceForCurrency(final CurrencyUnit currency) {
        synchronized (payments) {
            return payments.get(currency);
        }

    }

    /**
     * Gets all of {@link CurrencyUnit} from store.
     * Method is synchronized.
     *
     * @return Set of all currencies in store
     */
    @Override
    public Set<CurrencyUnit> getAllCurrencies() {
        synchronized (payments) {
            return new HashSet<>(payments.keySet());
        }
    }

}
